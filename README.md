Documentation
========================

See _Event Aggregator Implementation.pdf_ in the **dist** folder.

Run Application
========================

- From the project root directory run this command line:

**java -jar ./dist/event-reader.jar -file  ${PATH}**

**${PATH}**: optional path to the events data file, by default an events.txt file located in same folder as the jar is expected.

- To configure logging change the *log4j2.xml* in *dist* and pass it as an command line parameter:
**java -Dlog4j.configurationFile=./dist/log4j2.xml -jar ./dist/event-reader.jar**

*Minimum requirements*: JRE (Java Runtime Environment) version **1.8** is expected

Build Project
========================

- Linux OS family:
**./mvnw package**
- Windows OS family:
**./mvnw.cmd package**

*Minimum requirements*: JDK (Java Development Kit) version **1.8** is expected
