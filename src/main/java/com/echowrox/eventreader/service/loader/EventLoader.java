package com.echowrox.eventreader.service.loader;

import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by amazimpaka on 2018-01-21
 */
public interface EventLoader {

    List<String> load(String[] parameters) throws URISyntaxException, IOException, ParseException;
}
