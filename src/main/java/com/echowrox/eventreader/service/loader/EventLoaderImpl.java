package com.echowrox.eventreader.service.loader;

import com.echowrox.eventreader.EventReader;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by amazimpaka on 2018-01-21
 */
public class EventLoaderImpl implements EventLoader {

    public static final URL DEFAULT_EVENT_DATA_FILE = EventReader.class.getClassLoader()
            .getResource("events.txt");


    public static final CommandLineParser PARSER = new DefaultParser();

    private static Options createOptions() {

        final Options options = new Options();

        options.addOption(Option.builder("file")
                .hasArg()
                .desc("events data file path")
                .build());

        options.addOption(Option.builder("D")
                .argName("property=value")
                .hasArgs()
                .valueSeparator()
                .build());

        options.addOption(Option.builder("help")
                .desc("help")
                .build());

        return options;
    }

    @Override
    public List<String> load(String[] parameters) throws URISyntaxException, IOException, ParseException {

        if (parameters == null) {
            throw new IllegalArgumentException("Parameters array is required.");
        }

        final Path path = getEventsFilePath(parameters);
        return Files.readAllLines(path);

    }

    private Path getEventsFilePath(String[] parameters) throws ParseException {
        final CommandLine line = PARSER.parse(createOptions(), parameters);
        final String fileName = line.hasOption("file") ? line.getOptionValue("file") : null;
        if (fileName != null) {
            //Try to use file file provided as input parameter
            return Paths.get(fileName);
        } else {
            //Try to use default data in dist folder
            return FileSystems.getDefault().getPath(new File("dist/events.txt").getAbsolutePath());
        }
    }
}
