package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class LastEventByTypeProcessor implements EventProcessor<Event> {

    private static final Logger LOGGER = LogManager.getLogger(LastEventByTypeProcessor.class);

    private static final Map<EventType, Event> CACHE = Collections.synchronizedMap(new EnumMap<>(EventType.class));


    @Override
    public void process(Event event) {
            LOGGER.debug("handle event: " + event);

            final EventType eventType = event.getType();

        synchronized (this){
            final Event cachedEvent = CACHE.get(eventType);
            if (cachedEvent == null || cachedEvent.getTimestamp() < event.getTimestamp()) {
                //Save last event by occurrence and type
                CACHE.put(eventType, event);
            }
        }

    }

    @Override
    public void reportResults(PrintStream printStream) {
        printStream.println("--------------------------------------------------------------------");
        printStream.println("[2] Last occurrence of each event type");
        printStream.println("--------------------------------------------------------------------");
        this.getResults().forEach(printStream::println);
    }

    @Override
    public List<Event> getResults() {
        final List<Event> events = new ArrayList<>(CACHE.values());

        //Reverse sorting comparator
        Collections.sort(events, (o1, o2) -> Long.compare(o2.getTimestamp(), o1.getTimestamp()));

        return events;
    }

    @Override
    public synchronized void resetCache() {
        CACHE.clear();
    }

}
