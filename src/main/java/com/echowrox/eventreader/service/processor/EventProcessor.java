package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.model.Event;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by amazimpaka on 2018-01-20
 */
public interface EventProcessor<R> {

    void process(Event event);

    void reportResults(PrintStream printStream);

    List<R> getResults();

    void resetCache();

}
