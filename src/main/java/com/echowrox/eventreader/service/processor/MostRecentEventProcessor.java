package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.model.Event;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class MostRecentEventProcessor  implements EventProcessor<Event> {

    private static final Logger LOGGER = LogManager.getLogger(MostRecentEventProcessor.class);

    public static final int MAX_SIZE = 5;

    private static final List<Event> CACHE = Collections.synchronizedList(new ArrayList<>());


    @Override
    public void process(Event event) {
        LOGGER.debug("handle event: " + event);

        synchronized (this){
            //Save last event by occurrence
            CACHE.add(event);

            //Reverse sorting comparator
            Collections.sort(CACHE, (o1, o2) -> Long.compare(o2.getTimestamp(), o1.getTimestamp()));

            //Remove element beyond the fixed cache size
            if (CACHE.size() > MAX_SIZE) {
                CACHE.remove(CACHE.size() - 1);
            }
        }

    }

    @Override
    public void reportResults(PrintStream printStream) {
        printStream.println("--------------------------------------------------------------------");
        printStream.println("[3] Most recent five events (regardless of the type)");
        printStream.println("--------------------------------------------------------------------");
        this.getResults().forEach(printStream::println);
    }

    @Override
    public List<Event> getResults() {
        return Collections.unmodifiableList(CACHE);
    }

    @Override
    public synchronized void resetCache() {
        CACHE.clear();
    }
}
