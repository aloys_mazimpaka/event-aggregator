package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventCounter;
import com.echowrox.eventreader.model.EventType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class EventCounterProcessor implements EventProcessor<EventCounter> {

    private static final Logger LOGGER = LogManager.getLogger(EventCounterProcessor.class);

    private static final Map<EventType, EventCounter> CACHE = Collections.synchronizedMap(new EnumMap<>(EventType.class));

    @Override
    public void process(Event event) {
        LOGGER.debug("handle event: " + event);

        //Increment event occurrence count
        final EventType eventType = event.getType();
        synchronized (this){
            final EventCounter eventCounter = CACHE.computeIfAbsent(eventType, EventCounter::new);
            eventCounter.setCount(eventCounter.getCount() + 1);
        }
    }


    @Override
    public void reportResults(PrintStream printStream) {
        printStream.println("--------------------------------------------------------------------");
        printStream.println("[1] Top three events and their number of occurrences");
        printStream.println("--------------------------------------------------------------------");

        final List<EventCounter> results = getResults();
        final List<EventCounter> counters = results.size() > 3 ? results.subList(0, 3) : results;
        counters.forEach(printStream::println);
    }

    @Override
    public List<EventCounter> getResults() {
        final List<EventCounter> counters = new ArrayList<>();
        CACHE.forEach(((eventType, eventCounter) -> counters.add(eventCounter)));

        //Reverse sorting comparator
        Collections.sort(counters, (o1, o2) -> Integer.compare(o2.getCount(), o1.getCount()));

        return counters;
    }

    @Override
    public synchronized void resetCache() {
        CACHE.clear();
    }
}
