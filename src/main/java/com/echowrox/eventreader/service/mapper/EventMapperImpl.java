package com.echowrox.eventreader.service.mapper;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by amazimpaka on 2018-01-20
 */
public class EventMapperImpl implements EventMapper {

    public static final String SEPARATOR = " ";
    private static final Logger LOGGER = LogManager.getLogger(EventMapperImpl.class);

    @Override
    public Event parse(String message) {
        LOGGER.debug("Parsing message: " + message);

        if (message == null || message.trim().length() < 1) {
            throw new IllegalArgumentException("Invalid parameter: message cannot be null or blank");
        }

        final String[] tokens = message.split(SEPARATOR, 3);

        if (tokens.length != 3) {
            throw new IllegalArgumentException("Invalid parameter: message must be in format [TIMESTAMP EVENT MESSAGE] in event: " + message);
        }


        final long eventTimestamp = Long.parseLong(tokens[0]);
        final EventType eventType = EventType.valueOf(tokens[1]);
        final String eventMessage = tokens[2];

        final Event event = new Event(eventTimestamp, eventType);
        event.setMessage(eventMessage);

        //validate event
        validate(event);

        return event;

    }


    @Override
    public void validate(Event event) {
        if (event == null) {
            throw new IllegalArgumentException("Invalid event - should be null");
        }

        if (event.getType() == null) {
            throw new IllegalArgumentException("Invalid event type- should be null");
        }

        if (event.getTimestamp() <= 0) {
            throw new IllegalArgumentException("Invalid event timestamp - should be not negative: " + event.getTimestamp());
        }

        if (event.getMessage() == null) {
            throw new IllegalArgumentException("Invalid event message - should be null or blank");
        }

        if (event.getMessage().trim().isEmpty()) {
            throw new IllegalArgumentException("Invalid event message - should be blank");
        }

    }
}
