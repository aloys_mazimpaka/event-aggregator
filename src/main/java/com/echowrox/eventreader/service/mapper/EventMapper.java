package com.echowrox.eventreader.service.mapper;

import com.echowrox.eventreader.model.Event;

/**
 * Created by amazimpaka on 2018-01-20
 */
public interface EventMapper {

    Event parse(String message);

    void validate(Event event);
}
