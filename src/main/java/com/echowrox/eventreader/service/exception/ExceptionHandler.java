package com.echowrox.eventreader.service.exception;

import java.io.PrintStream;

/**
 * Created by amazimpaka on 2018-01-25
 */
public interface ExceptionHandler {

    void onException(String eventMessage, Exception exception);

    void reportErrors(PrintStream printStream, int processedEvents);

    int getErrorsCount();

    void reset();

}
