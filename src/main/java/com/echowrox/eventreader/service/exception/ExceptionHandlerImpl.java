package com.echowrox.eventreader.service.exception;

import com.echowrox.eventreader.EventReader;
import com.echowrox.eventreader.model.ExceptionInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by amazimpaka on 2018-01-25
 */
public class ExceptionHandlerImpl implements ExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(EventReader.class);

    private static final Map<String, ExceptionInfo> ERRORS = new LinkedHashMap<>();


    @Override
    public void onException(String eventMessage, Exception exception) {
        LOGGER.error(exception.getMessage(), exception);

        synchronized (this) {
            ERRORS.put(eventMessage, new ExceptionInfo(exception));
        }

    }


    @Override
    public void reportErrors(PrintStream printStream, int processedEvents) {
        final int errorsCount = ERRORS.size();
        final double successRate = 100.0 * (processedEvents - errorsCount) / processedEvents;

        printStream.println("--------------------------------------------------------------------");
        printStream.println("[4] Exceptions Report");
        printStream.println("--------------------------------------------------------------------");

        ERRORS.forEach((eventMessage, exceptionInfo) -> printStream.println(exceptionInfo + " -> Event Message: " + eventMessage));

        printStream.println(" # Processed Lines: " + processedEvents);
        printStream.println(" # Occurred Errors: " + errorsCount);
        printStream.println(" # Success Rate(%): " + String.format("%.2f", successRate));

    }

    @Override
    public int getErrorsCount() {
        return ERRORS.size();
    }

    @Override
    public void reset() {
        ERRORS.clear();
    }
}
