package com.echowrox.eventreader;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventCounter;
import com.echowrox.eventreader.service.exception.ExceptionHandler;
import com.echowrox.eventreader.service.exception.ExceptionHandlerImpl;
import com.echowrox.eventreader.service.loader.EventLoader;
import com.echowrox.eventreader.service.loader.EventLoaderImpl;
import com.echowrox.eventreader.service.mapper.EventMapper;
import com.echowrox.eventreader.service.mapper.EventMapperImpl;
import com.echowrox.eventreader.service.processor.EventCounterProcessor;
import com.echowrox.eventreader.service.processor.EventProcessor;
import com.echowrox.eventreader.service.processor.LastEventByTypeProcessor;
import com.echowrox.eventreader.service.processor.MostRecentEventProcessor;

import java.util.List;

public class EventReader {

    private static final EventLoader EVENT_LOADER = new EventLoaderImpl();

    private static final EventMapper EVENT_MAPPER = new EventMapperImpl();

    private static final ExceptionHandler EXCEPTION_HANDLER = new ExceptionHandlerImpl();

    private static final EventProcessor<EventCounter> EVENT_COUNTER_PROCESSOR = new EventCounterProcessor();
    private static final EventProcessor<Event> LAST_EVENT_BY_TYPE_PROCESSOR = new LastEventByTypeProcessor();
    private static final EventProcessor<Event> MOST_RECENT_EVENT_PROCESSOR = new MostRecentEventProcessor();


    public static void main(String[] args) throws Exception {

        final List<String> lines = EVENT_LOADER.load(args);

        lines.parallelStream().forEach(EventReader::handleEvent);

        printResults();

        printErrors(lines.size());
    }


    public static void handleEvent(String eventMessage) {
        try {
            final Event event = EVENT_MAPPER.parse(eventMessage);

            EVENT_COUNTER_PROCESSOR.process(event);
            LAST_EVENT_BY_TYPE_PROCESSOR.process(event);
            MOST_RECENT_EVENT_PROCESSOR.process(event);

        } catch (Exception exception) {
            EXCEPTION_HANDLER.onException(eventMessage, exception);
        }
    }

    @SuppressWarnings("squid:S106")
    public static void printResults() {
        EVENT_COUNTER_PROCESSOR.reportResults(System.out);
        LAST_EVENT_BY_TYPE_PROCESSOR.reportResults(System.out);
        MOST_RECENT_EVENT_PROCESSOR.reportResults(System.out);
    }

    @SuppressWarnings("squid:S106")
    private static void printErrors(int lineCount) {
        EXCEPTION_HANDLER.reportErrors(System.out, lineCount);
    }


}