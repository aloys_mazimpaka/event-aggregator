package com.echowrox.eventreader.model;

/**
 * Created by amazimpaka on 2018-01-25
 */
public class ExceptionInfo {

    private final Class<?> type;
    private final String message;
    private final StackTraceElement stackTraceElement;


    public ExceptionInfo(Throwable exception) {
        super();
        this.type = exception.getClass();
        this.message = exception.getMessage();
        this.stackTraceElement = exception.getStackTrace()[0];
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExceptionInfo{");
        sb.append("type=").append(type);
        sb.append(", message='").append(message).append('\'');
        sb.append(", stackTraceElement=").append(stackTraceElement);
        sb.append('}');
        return sb.toString();
    }
}
