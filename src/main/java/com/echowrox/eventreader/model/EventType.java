package com.echowrox.eventreader.model;

/**
 * Created by amazimpaka on 2018-01-20
 */
public enum EventType {
    INFO,
    TRACE,
    WARN,
    ALARM,
    FAILURE,
    STARTED,
    ENDED;
}
