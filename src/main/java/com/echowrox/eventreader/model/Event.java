package com.echowrox.eventreader.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;


/**
 * Created by amazimpaka on 2018-01-20
 */
public final class Event {

    private final long timestamp;

    private final EventType type;
    private final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy:HH:mm:ss z");
    private String message;


    public Event(long timestamp, EventType type) {
        this.timestamp = timestamp;
        this.type = type;
    }

    public EventType getType() {
        return type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        Objects.requireNonNull(message);
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (getTimestamp() != event.getTimestamp()) return false;
        return getType() == event.getType();
    }

    @Override
    public int hashCode() {
        int result = (int) (getTimestamp() ^ (getTimestamp() >>> 32));
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(type);
        sb.append(" ").append(dateFormat.format(new Date(timestamp)));
        sb.append(" ").append(message);
        return sb.toString();
    }

    public String toStringPlain() {
        final StringBuilder sb = new StringBuilder();
        sb.append(timestamp);
        sb.append(" ").append(type);
        sb.append(" ").append(message);
        return sb.toString();
    }

}
