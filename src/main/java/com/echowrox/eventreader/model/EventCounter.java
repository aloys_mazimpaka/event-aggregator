package com.echowrox.eventreader.model;

/**
 * Created by amazimpaka on 2018-01-21
 */
public final class EventCounter {

    private final EventType eventType;

    private int count;

    public EventCounter(EventType eventType) {
        this.eventType = eventType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public EventType getEventType() {
        return eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventCounter that = (EventCounter) o;

        return getEventType() == that.getEventType();
    }

    @Override
    public int hashCode() {
        return getEventType() != null ? getEventType().hashCode() : 0;
    }

    @Override
    public String toString() {
        return eventType + " : " + count;
    }
}
