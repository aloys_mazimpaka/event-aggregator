package com.echowrox.eventreader;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventCounter;
import com.echowrox.eventreader.service.exception.ExceptionHandler;
import com.echowrox.eventreader.service.exception.ExceptionHandlerImpl;
import com.echowrox.eventreader.service.processor.EventCounterProcessor;
import com.echowrox.eventreader.service.processor.EventProcessor;
import com.echowrox.eventreader.service.processor.LastEventByTypeProcessor;
import com.echowrox.eventreader.service.processor.MostRecentEventProcessor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Created by amazimpaka on 2018-01-23
 */
@SuppressWarnings("squid:S2187")
public class BaseTest {

    private static final Map<Object, Map<Field, Object>> FIELDS_CACHE = new HashMap<>();
    protected static EventProcessor<EventCounter> eventCounterProcessor = new EventCounterProcessor();
    protected static EventProcessor<Event> lastEventByTypeProcessor = new LastEventByTypeProcessor();
    protected static EventProcessor<Event> mostRecentEventProcessor = new MostRecentEventProcessor();
    protected static ExceptionHandler exceptionHandler = new ExceptionHandlerImpl();

    protected static void setFieldValue(Object object, String fieldName, Object fieldValue) {
        try {
            Optional<Field> foundField = searchFields(object.getClass(), fieldName);
            if (foundField.isPresent()) {
                Field field = foundField.get();
                Object currentValue = getFieldValue(object, field);
                setFiledValue(object, field, fieldValue);

                final Map<Field, Object> fields = FIELDS_CACHE.computeIfAbsent(object, (key) -> new HashMap<>());
                fields.put(field, currentValue);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static void setFiledValue(Object object, Field field, Object fieldValue) throws IllegalAccessException, NoSuchFieldException {
        final boolean accessible = field.isAccessible();
        final int modifiers = field.getModifiers();

        final Field modifiersField = Field.class.getDeclaredField("modifiers");


        try {
            modifiersField.setAccessible(true);
            field.setAccessible(true);

            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.set(object, fieldValue);
        } finally {
            field.setAccessible(accessible);

            modifiersField.setInt(field, modifiers);
            modifiersField.setAccessible(false);
        }
    }

    private static Object getFieldValue(Object object, Field field) throws IllegalAccessException {
        final boolean accessible = field.isAccessible();
        try {
            field.setAccessible(true);
            return field.get(object);
        } finally {
            field.setAccessible(accessible);
        }
    }

    private static Optional<Field> searchFields(Class<?> type, String fieldName) {

        System.out.format("--------------[#] %s - %s%n", type, fieldName);

        if (type == null || type.getClass().equals(Object.class)) {
            return Optional.empty();
        }

        Optional<Field> foundField = Optional.empty();
        final List<Field> allFields = new ArrayList<>();
        allFields.addAll(Arrays.asList(type.getFields()));
        allFields.addAll(Arrays.asList(type.getDeclaredFields()));

        boolean found = false;
        for (Field field : allFields) {
            System.out.format("\t\t\t [@] %s - %s%n", type, field.getName());
            if (field.getName().equals(fieldName)) {
                return Optional.of(field);
            }
        }

        if (!found) {
            return searchFields(type.getSuperclass(), fieldName);
        }


        return foundField;
    }

    @Before
    public void initialize() {

        eventCounterProcessor.resetCache();
        lastEventByTypeProcessor.resetCache();
        mostRecentEventProcessor.resetCache();
        exceptionHandler.reset();

        Assert.assertTrue(eventCounterProcessor.getResults().isEmpty());
        Assert.assertTrue(lastEventByTypeProcessor.getResults().isEmpty());
        Assert.assertTrue(mostRecentEventProcessor.getResults().isEmpty());
    }

    @After
    public void reset() {
        FIELDS_CACHE.forEach((object, fields) -> fields.forEach((field, value) -> {
            try {
                setFiledValue(object, field, value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }));

        FIELDS_CACHE.clear();
    }

}
