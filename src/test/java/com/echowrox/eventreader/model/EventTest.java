package com.echowrox.eventreader.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

/**
 * Created by amazimpaka on 2018-01-23
 */
public class EventTest {

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(Event.class)
                .withIgnoredFields("message", "dateFormat")
                .verify();
    }
}
