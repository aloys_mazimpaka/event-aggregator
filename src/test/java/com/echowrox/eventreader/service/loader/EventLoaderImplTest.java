package com.echowrox.eventreader.service.loader;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class EventLoaderImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private EventLoader eventLoader = new EventLoaderImpl();

    @Test
    public void loadDefaultDataFile() throws Exception {
        String[] parameters = {};
        List<String> lines = eventLoader.load(parameters);
        Assert.assertEquals(251, lines.size());
    }


    @Test
    public void loadGiventDataFile() throws Exception {
        String[] parameters = {"-file", "./src/test/resources/test-events.txt"};
        List<String> lines = eventLoader.load(parameters);
        Assert.assertEquals(10, lines.size());
    }


    @Test
    public void loadNullDataFile() throws Exception {

        expectedException.expect(IllegalArgumentException.class);

        String[] parameters = null;
        List<String> lines = eventLoader.load(parameters);
    }


}
