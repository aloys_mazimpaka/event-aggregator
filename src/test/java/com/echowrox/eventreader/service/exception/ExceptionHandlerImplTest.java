package com.echowrox.eventreader.service.exception;

import com.echowrox.eventreader.BaseTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * Created by amazimpaka on 2018-01-25
 */
public class ExceptionHandlerImplTest extends BaseTest {

    @Test
    public void testOnException() throws Exception {

        exceptionHandler.onException(RandomStringUtils.random(10), new RuntimeException());
        Assert.assertEquals(1, exceptionHandler.getErrorsCount());

        final PrintStream printStream = Mockito.mock(PrintStream.class);
        exceptionHandler.reportErrors(printStream, 10);
        Mockito.verify(printStream, Mockito.times(7)).println(anyString());
    }

}
