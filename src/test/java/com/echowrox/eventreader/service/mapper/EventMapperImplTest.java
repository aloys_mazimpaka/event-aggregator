package com.echowrox.eventreader.service.mapper;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class EventMapperImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private EventMapper eventMapper = new EventMapperImpl();

    @Test
    public void parseValidMessage() {
        Event event = eventMapper.parse("100 STARTED Main processes started");

        Assert.assertNotNull(event);
        Assert.assertEquals(EventType.STARTED, event.getType());
        Assert.assertEquals(100, event.getTimestamp());
        Assert.assertEquals("Main processes started", event.getMessage());
    }

    @Test
    public void parseNullMessage() {
        expectedException.expect(IllegalArgumentException.class);
        eventMapper.parse(null);
    }

    @Test
    public void parseEmptyMessage() {
        expectedException.expect(IllegalArgumentException.class);
        eventMapper.parse("");
    }

    @Test
    public void parseBlankMessage() {
        expectedException.expect(IllegalArgumentException.class);
        eventMapper.parse(" ");
    }


    @Test
    public void parseWrongFormatMessage() {
        expectedException.expect(IllegalArgumentException.class);
        eventMapper.parse("100 STARTED");
    }

    @Test
    public void validateNullEvent() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid event - should be null");

        eventMapper.validate(null);
    }

    @Test
    public void validateInvalidTimestamp() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid event timestamp - should be not negative: -100");

        eventMapper.validate(new Event(-100, EventType.STARTED));
    }

    @Test
    public void validateNullType() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid event type- should be null");

        eventMapper.validate(new Event(100, null));
    }


    @Test
    public void validateNullMessage() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid event message - should be null");

        eventMapper.validate(new Event(100, EventType.INFO));
    }

    @Test
    public void validateBlankMessage() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid event message - should be blank");

        final Event event = new Event(100, EventType.INFO);
        event.setMessage(" ");
        eventMapper.validate(event);
    }
}

