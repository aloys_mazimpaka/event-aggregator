package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.BaseTest;
import com.echowrox.eventreader.model.EventCounter;
import com.echowrox.eventreader.model.EventType;
import com.echowrox.eventreader.util.RandomEventGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class EventCounterProcessorTest extends BaseTest {


    @Test
    public void handleEvent() {

        RandomEventGenerator.generate(10, EventType.INFO)
                .forEach(eventCounterProcessor::process);

        RandomEventGenerator.generate(20, EventType.ALARM)
                .forEach(eventCounterProcessor::process);

        RandomEventGenerator.generate(30, EventType.WARN)
                .forEach(eventCounterProcessor::process);

        final List<EventCounter> results = eventCounterProcessor.getResults();
        Assert.assertEquals(3, results.size());

        final EventCounter eventCounter1 = results.get(0);
        Assert.assertEquals(30, eventCounter1.getCount());
        Assert.assertEquals(EventType.WARN, eventCounter1.getEventType());

        final EventCounter eventCounter2 = results.get(1);
        Assert.assertEquals(20, eventCounter2.getCount());
        Assert.assertEquals(EventType.ALARM, eventCounter2.getEventType());

        final EventCounter eventCounter3 = results.get(2);
        Assert.assertEquals(10, eventCounter3.getCount());
        Assert.assertEquals(EventType.INFO, eventCounter3.getEventType());

        eventCounterProcessor.reportResults(System.out);
    }


    @Test
    public void handleEventFewEvents() {

        //test event fewer than 3
        RandomEventGenerator.generate(2, 100, EventType.INFO)
                .forEach(eventCounterProcessor::process);


        final List<EventCounter> results = eventCounterProcessor.getResults();
        Assert.assertEquals(1, results.size());

        eventCounterProcessor.reportResults(System.out);
    }


}
