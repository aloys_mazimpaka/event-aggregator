package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.BaseTest;
import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import com.echowrox.eventreader.util.RandomEventGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class MostRecentEventProcessorTest extends BaseTest {

    @Test
    public void handleEvent() {

        RandomEventGenerator.generate(1, 1200, EventType.INFO)
                .forEach(mostRecentEventProcessor::process);

        RandomEventGenerator.generate(2, 2400, EventType.ALARM)
                .forEach(mostRecentEventProcessor::process);

        RandomEventGenerator.generate(3, 3600, EventType.WARN)
                .forEach(mostRecentEventProcessor::process);

        final List<Event> results = mostRecentEventProcessor.getResults();
        Assert.assertEquals(5, results.size());


        for (int i = 0; i < 5; i++) {
            final Event event = results.get(i);
            if (i < 3) {
                Assert.assertEquals(EventType.WARN, event.getType());
            } else {
                Assert.assertEquals(EventType.ALARM, event.getType());
            }
        }


        mostRecentEventProcessor.reportResults(System.out);
    }

}
