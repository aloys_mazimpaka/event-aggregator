package com.echowrox.eventreader.service.processor;

import com.echowrox.eventreader.BaseTest;
import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import com.echowrox.eventreader.util.RandomEventGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class LastEventByTypeProcessorTest extends BaseTest {

    @Test
    public void handleEvent() {


        RandomEventGenerator.generate(1, 1200, EventType.INFO)
                .forEach(lastEventByTypeProcessor::process);

        RandomEventGenerator.generate(1, 1250, EventType.INFO)
                .forEach(lastEventByTypeProcessor::process);

        RandomEventGenerator.generate(2, 2400, EventType.ALARM)
                .forEach(lastEventByTypeProcessor::process);

        RandomEventGenerator.generate(2, 2450, EventType.ALARM)
                .forEach(lastEventByTypeProcessor::process);

        RandomEventGenerator.generate(3, 3600, EventType.WARN)
                .forEach(lastEventByTypeProcessor::process);

        RandomEventGenerator.generate(3, 3650, EventType.WARN)
                .forEach(lastEventByTypeProcessor::process);

        final List<Event> results = lastEventByTypeProcessor.getResults();
        Assert.assertEquals(3, results.size());


        for (final Event event : results) {

            final EventType type = event.getType();
            switch (type) {
                case INFO: {
                    Assert.assertEquals(1250, event.getTimestamp());
                    break;
                }
                case ALARM: {
                    Assert.assertEquals(2450, event.getTimestamp());
                    break;
                }
                case WARN: {
                    Assert.assertEquals(3650, event.getTimestamp());
                    break;
                }
            }
        }

        lastEventByTypeProcessor.reportResults(System.out);
    }

}
