package com.echowrox.eventreader;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventCounter;
import com.echowrox.eventreader.model.EventType;
import com.echowrox.eventreader.service.loader.EventLoader;
import com.echowrox.eventreader.service.loader.EventLoaderImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by amazimpaka on 2018-01-22
 */
public class EventReaderTest extends BaseTest {

    private static EventLoader loader = new EventLoaderImpl();


    @Test
    public void testReadDefaultEvents() throws Exception {
        final String[] parameters = {};
        final List<String> lines = loadEvents(parameters);

        //Execute main function
        EventReader.main(parameters);

        //Check 1: verify that all lines were processed
        int total = 0;
        for (EventCounter eventCounter : eventCounterProcessor.getResults()) {
            total = total + eventCounter.getCount();
        }
        Assert.assertEquals(lines.size(), total);


        //Check 2: verify last event by type
        final Map<EventType, String> results = lastEventByTypeProcessor
                .getResults()
                .stream()
                .collect(Collectors.toMap(e -> e.getType(), e -> e.toStringPlain()));


        for (EventType eventType : EventType.values()) {
            String result = results.get(eventType);

            if (result != null) {
                for (String line : lines) {
                    if (line.contains(eventType.name())) {
                        Assert.assertEquals(line, result);
                        break;
                    }

                }
            }
        }


        //Check 3: verify most recent events
        final List<Event> mostRecentEvents = mostRecentEventProcessor.getResults();
        for (int k = 0; k < mostRecentEvents.size(); k++) {
            Assert.assertEquals(lines.get(k), mostRecentEvents.get(k).toStringPlain());
        }
    }

    @Test
    public void testReadErrorsEvents() throws Exception {
        final String[] parameters = {"-file", "src/test/resources/test-events-errors.txt"};
        final List<String> lines = loadEvents(parameters);
        Assert.assertEquals(12, lines.size());


        //Execute main function
        EventReader.main(parameters);
        final List<EventCounter> results = eventCounterProcessor.getResults();
        Assert.assertEquals(6, results.size());

        Assert.assertEquals(3, results.stream()
                .filter(ec -> ec.getEventType() == EventType.INFO)
                .findFirst().get().getCount());

        Assert.assertEquals(2, results.stream()
                .filter(ec -> ec.getEventType() == EventType.WARN)
                .findFirst().get().getCount());

        Assert.assertEquals(2, results.stream()
                .filter(ec -> ec.getEventType() == EventType.STARTED)
                .findFirst().get().getCount());

        Assert.assertEquals(1, results.stream()
                .filter(ec -> ec.getEventType() == EventType.TRACE)
                .findFirst().get().getCount());

        Assert.assertEquals(1, results.stream()
                .filter(ec -> ec.getEventType() == EventType.ENDED)
                .findFirst().get().getCount());

        Assert.assertEquals(1, results.stream()
                .filter(ec -> ec.getEventType() == EventType.ALARM)
                .findFirst().get().getCount());

    }


    private List<String> loadEvents(String[] parameters) throws Exception {
        final List<String> lines = loader.load(parameters);
        Collections.sort(lines);
        Collections.reverse(lines);

        return lines;
    }

}
