package com.echowrox.eventreader.util;

import com.echowrox.eventreader.model.Event;
import com.echowrox.eventreader.model.EventType;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amazimpaka on 2018-01-21
 */
public class RandomEventGenerator {

    public static List<Event> generate(int size) {
        final List<Event> events = new ArrayList<>();


        for (int i = 0; i < size; i++) {
            final long timestamp = RandomUtils.nextLong(100, 1000);

            final int index = RandomUtils.nextInt(0, EventType.values().length);
            final EventType type = EventType.values()[index];

            final Event event = generate(timestamp, type);

            events.add(event);
        }
        return events;
    }


    public static List<Event> generate(int size, EventType type) {
        final List<Event> events = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            events.add(generate(type));
        }
        return events;
    }

    public static List<Event> generate(int size, long timestamp, EventType type) {
        final List<Event> events = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            events.add(generate(timestamp, type));
        }
        return events;
    }


    public static Event generate(EventType type) {
        final long timestamp = RandomUtils.nextLong(100, 1000);
        return generate(timestamp, type);
    }


    public static Event generate(long timestamp, EventType type) {
        final Event event = new Event(timestamp, type);
        event.setMessage(RandomStringUtils.random(15));
        return event;
    }
}
